import librosa
import librosa.display
import numpy as np
import matplotlib.pyplot as plt


y, sr = librosa.load("songs/song.mp3")

fig, ax = plt.subplots(nrows=2, ncols=1, sharex=True)
D = librosa.amplitude_to_db(np.abs(librosa.stft(y)), ref=np.max)
img = librosa.display.specshow(D, y_axis="linear", x_axis="time", sr=sr, ax=ax[0])
ax[0].set(title="Linear-frequency power spectrogram")
ax[0].label_outer()

plt.show()

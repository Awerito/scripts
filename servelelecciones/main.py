import json
import httpx
import pandas as pd
import matplotlib.pyplot as plt

from time import sleep


def get_data():
    url = "https://servelelecciones.cl/data/elecciones_presidente/computo/global/19001.json"
    headers = {
        "Connection": "keep-alive",
        "Accept": "application/json, text/plain, */*",
        "Cache-Control": "no-cache",
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        "Referer": "https://servelelecciones.cl/",
        "Accept-Language": "en-US,en;q=0.9,es;q=0.8",
    }

    req = httpx.get(url, headers=headers)
    data = req.json()

    clean_data = list()
    for entry in data["data"]:
        clean = {
            key: value for key, value in zip(data["title"].values(), entry.values())
        }
        del clean[None]
        del clean["Partido"]
        del clean["Candidatos"]
        del clean["Electo"]
        clean_data.append(clean)

    df = pd.read_json(json.dumps(clean_data))

    df.rename(
        columns={
            "Nombre de los Candidatos": "candidates",
            "Votos": "votes",
            "Porcentaje": "percentage",
        },
        inplace=True,
    )
    df.candidates = df.candidates.str[3:]
    df = df.set_index("candidates")
    df.percentage = df.percentage.str.replace(",", ".")
    df.percentage = df.percentage.str[:-1]
    df.percentage = df.percentage.astype("float64")

    return df


if __name__ == "__main__":

    while True:
        df = get_data()
        df.plot.pie(y="percentage", autopct="%1.0f%%", legend=False, figsize=(10,9))
        plt.savefig("plot.png")
        sleep(60)

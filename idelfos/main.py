#!/home/awe/.local/share/virtualenvs/idelfos-vk7BloBe/bin/python3.9
#-*- coding:utf-8 -*-

import re
import httpx
import pandas as pd

from pprint import pprint

from secrets import headers, data


url = "http://idelfos.ulagos.cl:8080/idelfos/faces/cal/calf691500.jspx"
req = httpx.post(url, headers=headers, data=data)

if req.status_code == 200:
    df = pd.read_html(req.text)
    table = df[3].iloc[:-1, :]
    table = table.drop(
        columns=[
            "Seleccionar",
            "% Asistencia",
            "Periodo",
            "Codigo",
            "Sección",
            "Carrera",
            "Plan de Estudio",
        ],
        axis=1,
    )
    table["Nota Final"] = table["Nota Final"].astype("float")
    table.loc[table["Nota Final"] == 7.0, "Nota Final"] = 70.0

    # General mean
    marks = table[table["Nota Final"] >= 40]["Nota Final"].dropna()
    print(f"\nPromedio total carrera: {marks.mean()/10:.2f}\n")

    # Open courses
    open_courses = table[table["Situación"] == "ABIERTO"]["Curso Académico"].str.capitalize().to_list()
    print("Ramos abiertos:")
    for course in open_courses:
        print(f"\t{course}")
    print()

#!/usr/bin/env python3
#-*- coding:utf-8 -*-
import argparse
from random import randint


def simulation(n=6):
    i, x = 0, list()
    while len(set(x)) != n:
        i += 1
        x.append(randint(1, n))

    return x, i


def formula(n=6):
    return n * sum([1 / i for i in range(1, n + 1)])


def main(n=6, rounds=1000):
    throws = list()
    for _ in range(int(rounds)):
        result = simulation(int(n))
        throws.append(result)

        aux = [x[1] for x in throws]

    print(f"Experiment: {sum(aux) / len(aux)}")
    print(f"Analytic: {formula(int(n))}")


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Simulate throws")
    parser.add_argument(
        "range", metavar="n", type=int, help="Number of elements", default=6
    )
    parser.add_argument("-i", default=100, type=int, help="Iterations")

    args = parser.parse_args()
    main(args.range, args.i)

#!/bin/sh

_get_request () {
	# Get search query from youtube
	curl -f "https://www.youtube.com/results?search_query=bring+me+to+life+guitar+track" -s -L \
		"$@" \
		-H "User-Agent: $useragent" \
		-H 'Accept-Language: en-US,en;q=0.9' \
		--compressed
}

_youtube_get_json (){
       # Separates the json embedded in the youtube html page
       # * removes the content after ytInitialData
       # * removes all newlines and trims the json out
       sed -n '/var *ytInitialData/,$p' |
               tr -d '\n' |
               sed -E ' s_^.*var ytInitialData ?=__ ; s_;</script>.*__ ;'
}

_get_request > /tmp/test/youtube.html

_youtube_get_json < /tmp/test/youtube.html > youtube.json

def gen_words():
    with open(f"words.txt", "r") as f:
        words = f.read().split("\n")
    print(len(words))
    return words


def remove_letters(words, letters):
    for letter in letters:
        words = [word for word in words if letter not in word]
    print(len(words))
    return words


def has_letter_in(words, letter, pos):
    words = [word for word in words if letter in word[pos]]
    print(len(words))
    return words


def not_has_letter_in(words, letter, pos):
    words = [word for word in words if letter in word and letter != word[pos]]
    print(len(words))
    return words

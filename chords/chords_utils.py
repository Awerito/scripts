import httpx
import asyncio

async def requests(urls):

    SUCCESS = 200
    async with httpx.AsyncClient() as client:
        tasks = (client.get(url) for url in urls)
        reqs = await asyncio.gather(*tasks)

    return [req for req in reqs if req.status_code == SUCCESS]

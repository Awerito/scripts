import re
import httpx
import asyncio

from chords_utils import requests


EMPTY_STR = str()


def file_name(url):

    return "_".join(url.split("/")[4:6])[:-5] + "txt"


def parse_lyrics(text):

    data = re.findall(r"(?:<div id=t_body><PRE>)((?:.*[\n]?)+)", text)[0]
    # Do this after the regex because the regex gets tu slow
    lyrics = data[: data.index("</PRE></div>")]
    lyrics = re.sub(r"(<\/?.*?>)", EMPTY_STR, lyrics)
    lyrics = lyrics.replace(chr(0x92), EMPTY_STR)
    return lyrics


if __name__ == "__main__":

    urls = [
        "https://acordes.lacuerda.net/camilo_sesto/vivir_asi_es_morir_de_amor-3.shtml",
        "https://acordes.lacuerda.net/juan_luis_guerra/bachata_en_fukuoka-4.shtml",
        "https://acordes.lacuerda.net/fito__fitipaldis/por_la_boca_vive_el_pez-4.shtml",
        "https://acordes.lacuerda.net/camilo_sesto/getsemani.shtml",
    ]

    data = asyncio.run(requests(urls))
    for entry in data:
        lyrics = parse_lyrics(entry.text)
        with open(file_name(str(entry.url)), "w") as file:
            file.write(lyrics)

package main

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"regexp"
)

// lyrics regex
const (
	lyricsRE = `(\[.*?\].*?)(?:&quot\;,&quot\;)`
	acuteRE  = `&(.)acute\;`
	ntildeRE = `([aeiou])&ntilde;`
)

func parseLyrics(text []byte) string {

	lyrics := regexp.MustCompile(lyricsRE)
	matches := lyrics.FindAll(text, -1)

	var lyricsText []byte
	for k, data := range matches {
		if bytes.Contains(data, []byte("[ch]")) {
			lyricsText = matches[k]
		}
	}

	acute := regexp.MustCompile(acuteRE)
	lyricsText = acute.ReplaceAll(lyricsText, []byte("$1\u0301"))

	ntilde := regexp.MustCompile(ntildeRE)
	lyricsText = ntilde.ReplaceAll(lyricsText, []byte("$1\u0301"))

	lyricsText = bytes.ReplaceAll(lyricsText, []byte("\\n"), []byte("\n"))
	lyricsText = bytes.ReplaceAll(lyricsText, []byte("\\r"), []byte("\r"))
	lyricsText = bytes.ReplaceAll(lyricsText, []byte("[ch]"), []byte(""))
	lyricsText = bytes.ReplaceAll(lyricsText, []byte("[/ch]"), []byte(""))
	lyricsText = bytes.ReplaceAll(lyricsText, []byte("[tab]"), []byte(""))
	lyricsText = bytes.ReplaceAll(lyricsText, []byte("[/tab]"), []byte(""))

	return string(lyricsText)
}

func main() {
	//urls := []string{
	//"https://tabs.ultimate-guitar.com/tab/luis-fonsi/despacito-chords-1932541",
	//"https://tabs.ultimate-guitar.com/tab/rizky-ayuba/kimi-no-toriko-chords-3287060",
	//"https://tabs.ultimate-guitar.com/tab/sonata-arctica/shy-chords-923135",
	//"https://tabs.ultimate-guitar.com/tab/metallica/master-of-puppets-chords-98211",
	//}
	resp, err := http.Get("https://tabs.ultimate-guitar.com/tab/luis-fonsi/despacito-chords-1932541")
	if err != nil {
		panic("Error fetching page")
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic("Error fetching page")
	}

	lyrics := parseLyrics(body)
	fmt.Println(lyrics)
}

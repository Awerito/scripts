import re
import httpx
import asyncio

from chords_utils import requests


def file_name(url):

    return "_".join(url.split("/")[5:7]).replace("-", "_") + ".txt"


def parse_lyrics(text):
    TILDE = u"\u0301"

    # &quot;,&quot; end of params json thing
    data = re.findall(r"(\[.*?\].*?)(?:&quot;,&quot;)", text)
    lyrics = [item for item in data if "[ch]" in item and "[/ch]" in item][0]

    lyrics = re.sub(r"&(.)acute;", r"\1" + TILDE, lyrics)
    lyrics = re.sub(r"([aeiou])&ntilde;", r"\1" + TILDE, lyrics)

    lyrics = (
        lyrics.replace("\\n", "\n")
        .replace("\\r", "\r")
        .replace("[ch]", "")
        .replace("[/ch]", "")
        .replace("[tab]", "")
        .replace("[/tab]", "")
    )
    return lyrics


if __name__ == "__main__":

    urls = [
        "https://tabs.ultimate-guitar.com/tab/luis-fonsi/despacito-chords-1932541",
        "https://tabs.ultimate-guitar.com/tab/rizky-ayuba/kimi-no-toriko-chords-3287060",
        "https://tabs.ultimate-guitar.com/tab/sonata-arctica/shy-chords-923135",
        "https://tabs.ultimate-guitar.com/tab/metallica/master-of-puppets-chords-98211",
    ]

    data = asyncio.run(requests(urls))
    for entry in data:
        lyrics = parse_lyrics(entry.text)
        with open(file_name(str(entry.url)), "w") as file:
            file.write(lyrics)

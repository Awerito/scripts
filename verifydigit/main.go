package main

import "fmt"

func reverse(str []rune) {
	size := len(str)
	for i := 0; i < size/2; i++ {
		str[i] = str[i] ^ str[size-i-1]
		str[size-i-1] = str[i] ^ str[size-i-1]
		str[i] = str[size-i-1] ^ str[i]
	}
}

func main() {
	rut := []rune("19759158")
	reverse(rut)

	sum := 0
	for i, char := range rut {
		if int(char) != 46 {
			sum += (int(char) - 48) * (i%6 + 2)
		}
	}

	fmt.Println(11 - sum%11)
}

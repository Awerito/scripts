from itertools import cycle


def verificator_digit(rut="12345678"):

    sequence = zip([int(n) for n in rut[::-1]], cycle(range(2, 8)))
    total = sum([digit * seq for digit, seq in sequence])
    verificator = 11 - total % 11

    if verificator == 11:
        verificator = 0
    if verificator == 10:
        verificator = "k"

    return verificator


print(verificator_digit(input("Rut: ")))

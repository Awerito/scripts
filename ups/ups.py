import httpx
from secrets import tracking_number, headers

url = "https://www.ups.com/track/api/Track/GetStatus?loc=en_US"
data = (
    '{"Locale":"en_US","TrackingNumber":["%s"],"Requester":"wt/trackdetails","returnToValue":""}'
    % tracking_number
)

response = httpx.post(url, headers=headers, data=data)
json_data = response.json()

print(json_data["trackDetails"][0]["packageStatus"])

#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""
Word unshuffler script
"""

from pprint import pprint
from random import shuffle
from string import ascii_uppercase


def strtodict(word):
    return {key: list(word).count(key) for key in set(word)}


words = list()

for letter in list(ascii_uppercase):
    with open(f"WordsV2.0/{letter}.txt", "r") as f:
        data = f.read()
    words.extend(data.split("\n"))

words = [
    word.replace("á", "a")
    .replace("é", "e")
    .replace("í", "i")
    .replace("ó", "o")
    .replace("ú", "u")
    for word in words
]
print("Dictionary entries:", len(words))

# TODO: remove tildes

# with open(f"WordsV1.0.txt", "r") as f:
# data = f.read()
# words = data.split("\n")

search = list(input("Ingrese la palabra a buscar: "))
shuffle(search)
search = "".join(search)
print("\n")
print(search)

input("Press enter")

search_dict = strtodict(search)
search_length = len(search)
search_set = set(search)
print("Search data")
print("\nLength:", search_length, "\nSet:", search_set, "\nDict:", search_dict)

words = [word for word in words if set(word) == search_set]
print("\nSet filter")
pprint(words)

if len(words) != 1:
    words = [word for word in words if len(word) == search_length]
    print("\nLength filter")
    pprint(words)

    if len(words) != 1:
        words = [word for word in words if strtodict(word) == search_dict]
        print("\nDict filter")
        pprint(words)

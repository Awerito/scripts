#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
Scrapper for Spanish dictionary  from listapalabras.com
'''

import re
import httpx
import asyncio
from string import ascii_uppercase
from pprint import pprint


index = int(input("Letter: "))
SUCCESS = 200

uppercase = list(ascii_uppercase)
uppercase.insert(3, "CH")
uppercase.insert(13, "LL")

urls = [
    f"https://www.listapalabras.com/palabras-con.php?letra={letter}&total=s"
    for letter in uppercase[index:]
]

for letter, url in zip(uppercase[index:], urls):
    try:
        q = httpx.get(url)
        print(f"Conection to {url} done with status code: {q.status_code}")
        if q.status_code == SUCCESS:
            words = re.findall(r'https:\/\/dle.rae.es\/(.*)', q.text)
            q.close()
            with open(f"WordsV2.0/{letter}.txt", "w") as f:
                f.write("\n".join(words))
    except httpx.ReadTimeout:
        print(f"Fail to connect to {url} on letter {letter}: {uppercase.index(letter)}")

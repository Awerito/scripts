import re
from pprint import pprint

with open("words.txt", "r") as file:
    words = file.read().split("\n")

print(len(words))

results = [
    (word, max([len(i) for i in re.split(r"a|e|i|o|u|á|é|í|ó|ú", word)]))
    for word in words
]

results.sort(key=lambda x: x[1], reverse=True)
test = [word[0] for word in results if word[1] == 4]

pprint(test[:10])
pprint(len(test))
